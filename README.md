# Dezentrale Workadventure

This repository contains instructions and files to run workadventure on our server `play.world.dezentrale.cloud`. This installation may differ from others, as we have a special situation here:

* everything runs behind a reverse proxy server
* we don't have a public ip address on the server itself
* SSL terminates on this reverse proxy

The following domains are connected to our instance:

* world.dezentrale.cloud - will serve a welcome page
* play.world.dezentrale.cloud - serves the workadventure itself
* pusher, back and icon.world.dezentrale.cloud for internal usage

## Architecture Overview

```mermaid
stateDiagram-v2
    [*] --> ReverseProxy: HTTP(S)
    ReverseProxy --> Containers: HTTPS
    state Containers {
        [*] -->Front
        Front --> Back
        Back --> Front
        Front --> WWW
        note left of WWW: nginx, delivers a\nlittle landing page  
        Front --> Icon
        Icon --> Front
        Back --> Maps
        Pusher --> Front: Map Changes
        Maps --> Pusher: Maps
    }
    note right of Containers
        containers are orchestrated by traefik
    end note
    Containers --> [*] 
            
```

## Install and Run

* clone the workadventure itself from https://github.com/workadventure-xce/workadventure-xce/
* it is best practice to select a release and check that out via git: e.g. `git checkout xce`
* copy the `contrib-dz/` folder from this repo the the workadventure directory
* copy `.env.prod.template` to `.env` and add the correct configuration
* run `docker network create wkadv`, if the network does not exist right now
* run `docker-compose build` to build all docker containers out of the contrib-dz folder
* run `docker-compose up -d` to start the docker containers out of the contrib-dz folder
* go to https://play.world.dezentrale.cloud and have fun :)


## Operations

### Stop the containers

Go to the contrib-directory and run `docker-compose stop`.

### Debug

Use `docker ps` to find the running docker containers. With `docker <container id> logs` you can see specific logs from this container.

To see all live logs, don't start in daemon mode, just run `docker-compose up`.

## Maps

If you want to serve some maps with this server, copy those files in contrib-dz/www/maps.

If you experience stripes on your maps, you can run the [map optimizer](https://github.com/thecodingmachine/map-optimizer) provided by The Coding Machine. This will optimize your tiles used in your maps. As we sometimes experience strange things when we open optimized maps again in the map editor, it's probably better to include this step in a post-commit-thing. In github you can use the [map optimizer action](https://github.com/thecodingmachine/map-optimizer-action).

## nginx templates

We use nginx containers for serving things like maps or pages like https://world.dezentrale.cloud. For special configurations, we can use nginx templates stored in `contrib/nginx-templates`. You need to restart the docker images to get these changes live.

